

<hrml>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
        <title>Calculator</title>
    </head>
    <body>
    <div class="container">
        <div class="header">
           <h2 class="title"> A Simple Calculator </h2>
        </div>
        <div class="content">
            <div class="cal">
                <form method="post" action="index.php">

                    <label>First N0:</label>
                    <input type="text" name="firstNo" placeholder="e.g:10">
                    <label>Second No:</label>
                    <input type="text" name="secondNo" placeholder="e.g:5">
                    <br>
                    <div class="operation">
                        <input type="submit" name="add" value="ADDITION">
                        <input type="submit" name="sum" value="SUBTRACTION">
                        <input type="submit" name="mul" value="MULTIPLICATION">
                        <input type="submit" name="div" value="DIVISION">
                    </div>
                </form>
            </div>
        </div>
        <div class="footer">
            <p class="result">The Result is:</p>
        </div>
    </div>
    </body>
</hrml>