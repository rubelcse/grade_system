<?php
function someFunction()
{
}

$functionVariable = 'someFunction';

var_dump(is_callable($functionVariable, true, $callable_name));  // bool(true)
echo "<br>";

echo $callable_name, "\n";  // someFunction
echo "<br>";

//
//  Array containing a method
//

class someClass {

    function someMethod()
    {
    }

}

$anObject = new someClass();

$methodVariable = array($anObject, 'someMethod');

var_dump(is_callable($methodVariable, true, $callable_name));  //  bool(true)

echo $callable_name, "\n";  //  someClass::someMethod

