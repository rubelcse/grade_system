<?php
$str = "I love Bangladesh.";
$str_repeat = str_repeat($str,5);
echo $str_repeat."<br>";
$str_replace = str_replace("I","We",$str);
echo $str_replace."<br>";
$str_shuffle = str_shuffle($str);
echo $str_shuffle."<br>";

echo nl2br("One line.\nAnother line.");
echo "<br>";
$str_padding = str_pad($str,50,'love');
echo $str_padding."<br>";

$sub_replace = substr_replace($str,'world',5);
echo $sub_replace."<br>";
$str_possition = strpos($str,'l');
echo $str_possition;
