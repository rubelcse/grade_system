<?php
class A{
    public function foo(){
        if (isset($this)){
            echo "\$this is defined ( ";
            echo get_class($this);
            echo " )"."<br>";
        }else{
            echo "\$this is not defined"."<br>";
        }
    }
}

class B {
    public function bar(){
        A::foo();
    }
}
$obj = new A();
/*$obj3 = new A();
$obj3->foo();*/
$obj->foo();
A::foo();

$obj2 = new B();
$obj2->bar();
B::bar();