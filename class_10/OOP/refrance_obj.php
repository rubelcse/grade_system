<?php
class SimpleClass{

}
$instance = new SimpleClass();

$assigned   =  $instance;
$reference  =& $instance;

$instance->var = '$assigned will have this value';

$instance = null; // $instance and $reference become null

var_dump($instance);
var_dump($reference);
echo "<pre>";
var_dump($assigned);
echo (new DateTime())->format('Y');
